<?php 

declare (strict_types = 1);

namespace MDC\PickupPoints\Model;

use MDC\PickupPoints\Api\PickupPointsListInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * 
 */
class PickupPointsList implements PickupPointsListInterface
{
	
	function __construct(
		\MDC\PickupPoints\Model\PickupsFactory $pickupsModelFactory,
		\MDC\PickupPoints\Model\Data\PickupPointsDataFactory $pickupPointsData,
		\MDC\PickupPoints\Model\Data\PickupPointsObjFactory $pickupPointsObj,
		\MDC\PickupPoints\Helper\Data $helper
	)
	{
		$this->pickupsModelFactory = $pickupsModelFactory;
		$this->pickupPointsData = $pickupPointsData;
		$this->pickupPointsObj = $pickupPointsObj;
		$this->helper = $helper;
	}


	/**
     * List of pickup points added by admin     
     * @return \MDC\PickupPoints\Api\Data\PickupPointsDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException.
     */
	public function getPickupPointsList(){
		
		$pickupPointsEnabled = $this->helper->isPikcupPointsEnabled();

		$pickupPointsCollection = $this->pickupsModelFactory->create()
									->getCollection()
									->setOrder('pickup_point_id','DESC');
	
		$pickupPointsArray = [];
		foreach ($pickupPointsCollection as $key => $value) {
			$pickupPointsObjData = $this->pickupPointsObj->create();		
			$pickupPointsObjData->setPickuppointId($value->getPickupPointId());
			$pickupPointsObjData->setCountryId($value->getPickupCountry());

			$locationData = array($value->getPickupPointName(),$value->getPickupAddress()) ;
			$pickupPointsObjData->setStreet($locationData);
			$pickupPointsObjData->setCity($value->getPickupCity());
			$pickupPointsObjData->setLatitude($value->getPickupPointLat());
			$pickupPointsObjData->setLongitude($value->getPickupPointLong());

			$pickupPointsArray[] = $pickupPointsObjData;			
		}		

		$pickupPointsData = $this->pickupPointsData->create();
		$pickupPointsData->setPickupPoints($pickupPointsArray);

		if(!$pickupPointsEnabled){
			$pickupPointsArray = array();
			$pickupPointsData->setPickupPoints($pickupPointsArray);
		}
		
		return $pickupPointsData;
	}
}