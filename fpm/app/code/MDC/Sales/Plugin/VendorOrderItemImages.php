<?php 

namespace MDC\Sales\Plugin;

use Magento\Sales\Api\Data\OrderInterface;
use Magedelight\Sales\Api\Data\OrderItemImageInterface;
/**
 * 
 */
class VendorOrderItemImages extends \Magedelight\Sales\Plugin\VendorOrderItemImages
{
	
	/**
     * @var OrderItemImageInterfaceFactory
     */
    protected $orderItemImageInterfaceFactory;
    
    /**
     * @var Magento\Catalog\Helper\Product
     */
    protected $productHelper;
    
    /**
     * @param \MDC\Sales\Api\Data\OrderItemImageInterfaceFactory $orderItemImageInterfaceFactory
     * @param \Magento\Catalog\Helper\Product $productHelper
     */
    public function __construct(
        \Magedelight\Sales\Api\Data\OrderItemImageInterfaceFactory $orderItemImageInterfaceFactory,
        \Magento\Catalog\Helper\Product $productHelper
    ) {
        $this->orderItemImageInterfaceFactory = $orderItemImageInterfaceFactory;
        $this->productHelper = $productHelper;
    }

    /**
     * Get Items
     *
     * @return \Magento\Sales\Api\Data\OrderItemInterface[]
     */
    public function afterGetItems(
        \Magedelight\Sales\Api\Data\VendorOrderInterface $subject,
        $result
    ) {     	
         $variantImageData = [];
         $isVariantPorduct = false;
        foreach ($result as $item) {
            $variantImageData = $this->orderItemImageInterfaceFactory->create();

            if($item->getParentItem()){

                $variantImageData->setImageUrl($this->productHelper->getImageUrl(
                    $item->getProduct()
                 ));
                $isVariantPorduct = true;
            } 
        }
         
        foreach ($result as $item) {
            $imageData = $this->orderItemImageInterfaceFactory->create();
            $imageData->setImageUrl($this->productHelper->getImageUrl(
                $item->getProduct()
            ));
            $extensionAttributes = $item->getExtensionAttributes();
            // $extensionAttributes->setOrderItemImageData($imageData);
            
            if(isset( $variantImageData ) && $isVariantPorduct){
                $extensionAttributes->setOrderItemImageData($variantImageData); 
            }else{
                $extensionAttributes->setOrderItemImageData($imageData);
            }
           
            $item->setExtensionAttributes($extensionAttributes);
        }
        return $result;
    }
}