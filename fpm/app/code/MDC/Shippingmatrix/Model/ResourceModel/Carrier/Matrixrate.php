<?php

namespace MDC\Shippingmatrix\Model\ResourceModel\Carrier;

use Magento\Framework\Filesystem\DirectoryList;


class Matrixrate extends \Magedelight\Shippingmatrix\Model\ResourceModel\Carrier\Matrixrate
{
    
    public function getRate(\Magento\Quote\Model\Quote\Address\RateRequest $request, $zipRangeSet = false)
    {

        $requestQuoteId = 0;
        foreach ($request->getAllItems() as $item) {
            $requestQuoteId = $item->getQuoteId();
        }

        
        $requestAddressQuote = $this->quoteRepository->get($requestQuoteId);

        $customerCityType = $requestAddressQuote->getData("address_type");  //Is province city, 1= Yes, 0= No


        $adapter = $this->getConnection();
        $shippingData=[];
        $postcode = $request->getDestPostcode();
        $city = $request->getDestCity();
        if ($zipRangeSet && is_numeric($postcode)) {
            #  Want to search for postcodes within a range
            $zipSearchString = ' AND :postcode BETWEEN dest_zip AND dest_zip_to ';
        } else {
            $zipSearchString = " AND :postcode LIKE dest_zip ";
        }

        for ($j=0; $j<23; $j++) {
            $select = $adapter->select()->from(
                $this->getMainTable()
            )->where(
                'website_id = :website_id'
            )->order(
                ['price ASC', 'condition_from_value DESC']
            );

            $zoneWhere = '';
            $bind = [];
            switch ($j) {
                case 0: // vendor_id, country, region, city, postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = :city AND province_city = :province_city ".$zipSearchString;  // TODO Add city
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':city' => $request->getDestCity(),
                        ':postcode' => $request->getDestPostcode(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 1: // vendor_id, country, region, no city, postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = '*' ".$zipSearchString;
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':vendor_id' => $request->getVendorId(),
                        ':postcode' => $request->getDestPostcode(),
                    ];
                    break;
                case 2: // vendor_id, country, no region, city, postcode
                    $zoneWhere = "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ".$zipSearchString;
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':city' => $request->getDestCity(),
                        ':postcode' => $request->getDestPostcode(),
                        ':vendor_id' => $request->getVendorId(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 3: // vendor_id, country, state, city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = :city AND province_city = :province_city AND dest_zip='*'"; // TODO Add city search
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':city' => $request->getDestCity(),
                        ':vendor_id' => $request->getVendorId(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 4: // no vendor_id, country, state, city, postcode
                    $zoneWhere = "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ".$zipSearchString;
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':city' => $request->getDestCity(),
                        ':postcode' => $request->getDestPostcode(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 5: // vendor_id, country, state, no city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = '*' AND dest_zip='*'";
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                    ];
                    break;
                case 6: // vendor_id, country, no state, no city, postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = '*' ".$zipSearchString;
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                        ':country_id' => $request->getDestCountryId(),
                        ':postcode' => $request->getDestPostcode(),
                    ];
                    break;
                case 7: // no vendor_id, country, state, no city, postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = '*' ".$zipSearchString;
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':postcode' => $request->getDestPostcode(),
                    ];
                    break;
                case 8: // vendor_id, country, no state, city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city AND dest_zip = '*' ";
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                        ':country_id' => $request->getDestCountryId(),
                        ':city' => $request->getDestCity(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 9: // no vendor_id, country, state, city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city = :city AND province_city = :province_city AND dest_zip='*'";
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':city' => $request->getDestCity(),
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 10: // vendor_id, no country, no state, city, postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ".$zipSearchString;
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':city' => $request->getDestCity(),
                        ':vendor_id' => $request->getVendorId(),
                        ':postcode' => $request->getDestPostcode(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 11: // no vendor_id, country, no state, city, postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ".$zipSearchString;
                    $bind = [
                        ':city' => $request->getDestCity(),
                        ':country_id' => $request->getDestCountryId(),
                        ':postcode' => $request->getDestPostcode(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 12: // vendor_id, no country, no state, no city, postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city='*'".$zipSearchString;
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                        ':postcode' => $request->getDestPostcode(),
                    ];
                    break;
                case 13: // vendor_id, country, no state, no city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city='*' AND dest_zip='*'";
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                        ':vendor_id' => $request->getVendorId(),
                    ];
                    break;
                case 14: // no vendor_id, country, state, no city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = :region_id AND dest_city='*' AND dest_zip='*'";
                    $bind = [
                        ':region_id' => (int)$request->getDestRegionId(),
                        ':country_id' => $request->getDestCountryId(),
                    ];
                    break;
                case 15: // vendor_id, no country, no state, city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ";
                    $bind = [
                        ':city' => $request->getDestCity(),
                        ':vendor_id' => $request->getVendorId(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 16: // no vendor_id, country, no state, city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ";
                    $bind = [
                        ':city' => $request->getDestCity(),
                        ':country_id' => $request->getDestCountryId(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 17: // no vendor_id, no country, no state, city, postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city ".$zipSearchString;
                    $bind = [
                        ':city' => $request->getDestCity(),
                        ':postcode' => $request->getDestPostcode(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 18: // vendor_id, no country, no state, no city, no postcode
                    $zoneWhere =  "vendor_id = :vendor_id AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city='*' AND dest_zip='*'";
                    $bind = [
                        ':vendor_id' => $request->getVendorId(),
                    ];
                    break;
                case 19: // no vendor_id, country, no state, no city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = :country_id AND dest_region_id = '0' AND dest_city='*' AND dest_zip='*'";
                    $bind = [
                        ':country_id' => $request->getDestCountryId(),
                    ];
                    break;
                case 20: // no vendor_id, no country, no state, city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city = :city AND province_city = :province_city AND dest_zip='*'";
                    $bind = [
                        ':city' => $request->getDestCity(),
                        ':province_city' => $customerCityType,
                    ];
                    break;
                case 21: // no vendor_id, no country, no state, no city, postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city='*'".$zipSearchString;
                    $bind = [
                        ':postcode' => $request->getDestPostcode(),
                    ];
                    break;
                case 22: // no vendor_id, no country, no state, no city, no postcode
                    $zoneWhere =  "vendor_id = '0' AND dest_country_id = '0' AND dest_region_id = '0' AND dest_city ='*' AND dest_zip ='*'";
                    break;
            }

            $select->where($zoneWhere);

            $bind[':website_id'] = (int)$request->getWebsiteId();
            $bind[':condition_name'] = $request->getConditionMRName();
            $bind[':condition_value'] = $request->getData($request->getConditionMRName());
            
            $select->where('condition_name = :condition_name');
            $select->where('condition_from_value <= :condition_value');
            $select->where('condition_to_value >= :condition_value');            

           // $logger->info('\n-------------------------------------------\n');
           // $logger->info('WHERE:'.$zoneWhere);
           // $logger->info('\n-------------------------------------------\n');
           // $logger->info('J:'.$j);
           // $logger->info('Bind'.json_encode($bind));
           // $logger->info('\n____________________________________________\n');
           // $logger->info('Select:'.$select);        
           // $logger->info('\n-------------------------------------------\n');

            $results = $adapter->fetchAll($select, $bind);


            if (!empty($results)) {
                foreach ($results as $data) {
                    if (!array_key_exists($data['shipping_method'], $shippingData)) {
                        $shippingData[$data['shipping_method']] = $data;
                    }
                }
                break;
            }
        }
       // $logger->info('ShippingData'.json_encode($shippingData));
       // $logger->info('\n-------------------------------------------\n');
        return $shippingData;
    }

}
