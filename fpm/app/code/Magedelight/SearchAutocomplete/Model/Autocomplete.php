<?php
/**
 * Magedelight
 * Copyright (C) 2019 Magedelight <info@magedelight.com>
 *
 * @category Magedelight
 * @package Magedelight_SearchAutocomplete
 * @copyright Copyright (c) 2019 Mage Delight (http://www.magedelight.com/)
 * @license http://opensource.org/licenses/gpl-3.0.html GNU General Public License,version 3 (GPL-3.0)
 * @author Magedelight <info@magedelight.com>
 */

namespace Magedelight\SearchAutocomplete\Model;

use Magedelight\SearchAutocomplete\Api\AutocompleteInterface as RBAutocompleteInterface;
use Magento\Search\Model\AutocompleteInterface;

class Autocomplete implements RBAutocompleteInterface
{
    /**
     * @var  \Magento\Search\Model\AutocompleteInterface
     */
    private $autocomplete;

    /**
     * @var \Amasty\Xsearch\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $catCollFactory;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Page\CollectionFactory
     */
    private $cmsCollFactory;

    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Magento\Cms\Helper\Page
     */
    private $cmsHelper;

    /**
     * @var \Magento\Search\Model\ResourceModel\Query\CollectionFactory
     */
    private $searchQueryColl;

    /**
     * @var \Magento\Catalog\Api\CategoryRepositoryInterface
     */
    private $categoryRepo;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $priceHelper;

    private $listProduct;

    private $responseData = [];

    private $counter = 0;

    /**
     * @param \Magento\Search\Model\AutocompleteInterface                     $autocomplete
     * @param \Amasty\Xsearch\Helper\Data                                     $helper
     * @param \Magento\Store\Model\StoreManagerInterface                      $storeManager
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory  $productCollFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $catCollFactory
     * @param \Magento\Cms\Model\ResourceModel\Page\CollectionFactory         $cmsCollFactory
     * @param \Magento\Catalog\Helper\Image                                   $imageHelper
     * @param \Magento\Cms\Helper\Page                                        $cmsHelper
     * @param \Magento\Search\Model\ResourceModel\Query\CollectionFactory     $searchQueryColl
     * @param \Magento\Catalog\Api\CategoryRepositoryInterface                $categoryRepo
     * @param \Magento\Framework\Pricing\Helper\Data                          $priceHelper
     */
    public function __construct(
        \Magento\Search\Model\AutocompleteInterface $autocomplete,
        \Amasty\Xsearch\Helper\Data $helper,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $catCollFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $cmsCollFactory,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Cms\Helper\Page $cmsHelper,
        \Magento\Search\Model\ResourceModel\Query\CollectionFactory $searchQueryColl,
        \Magento\Catalog\Api\CategoryRepositoryInterface $categoryRepo,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Catalog\Block\Product\ListProduct $listProduct
    ) {
        $this->autocomplete = $autocomplete;
        $this->helper = $helper;
        $this->storeManager = $storeManager;
        $this->productCollFactory = $productCollFactory;
        $this->catCollFactory = $catCollFactory;
        $this->cmsCollFactory = $cmsCollFactory;
        $this->imageHelper = $imageHelper;
        $this->cmsHelper = $cmsHelper;
        $this->searchQueryColl = $searchQueryColl;
        $this->categoryRepo = $categoryRepo;
        $this->priceHelper = $priceHelper;
        $this->listProduct = $listProduct;
    }

    /**
     * {@inheritdoc}
     */
    public function getAutoItems($q)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $autocompleteData = $this->autocomplete->getItems();
        $isProductSearch = $this->helper->getModuleConfig('product/enabled');
        $isRecentSearch = $this->helper->getModuleConfig('recent_searches/enabled');
        $isPopularSearch = $this->helper->getModuleConfig('popular_searches/enabled');
        $isCategorySearch = $this->helper->getModuleConfig('category/enabled');
        $isCmsSearch = $this->helper->getModuleConfig('cms/enabled');

        $productSearchLimit = $this->helper->getModuleConfig('product_searches/limit');
        $categorySearchLimit = $this->helper->getModuleConfig('category_searches/limit');
        $cmsSearchLimit = $this->helper->getModuleConfig('cms_searches/limit');
        $popSearchLimit = $this->helper->getModuleConfig('popular_searches/limit');
        $recentSearchLimit = $this->helper->getModuleConfig('recent_searches/limit');

        $searchList = [];
        if ($q == "") {
            $this->getPopularSearch($this->responseData, $isPopularSearch, $q, $autocompleteData, $popSearchLimit);
            $this->getRecentSearch($this->responseData, $isRecentSearch, $q, $storeId, $recentSearchLimit);
        } else {
            $this->getProductSearch($this->responseData, $isProductSearch, $q, $storeId);
            $this->getCategorySearch($this->responseData, $isProductSearch, $q);
            $this->getCmsSearch($this->responseData, $isProductSearch, $q, $storeId);
            $this->getPopularSearch($this->responseData, $isPopularSearch, $q, $autocompleteData, $popSearchLimit);
            $this->getRecentSearch($this->responseData, $isRecentSearch, $q, $storeId, $recentSearchLimit);
        }
        return $this->responseData;
    }

    public function getItems($q)
    {

        $autocompleteData = $this->autocomplete->getItems();
        $responseData = [];
        foreach ($autocompleteData as $resultItem) {
            $responseData[] = $resultItem->toArray();
        }
        return $responseData;
    }

    private function getProductSearch($responseData, $isProductSearch, $q, $storeId)
    {
        if ($isProductSearch) {

            $baseUrl = $this->storeManager->getStore()->getBaseUrl();
            $storeCode = $this->storeManager->getStore()->getCode();

            $searchCriteriaUrl = "" . $baseUrl . "rest/" . $storeCode . "/V1/search?searchCriteria[requestName]=advanced_search_container&searchCriteria[filterGroups][0][filters][0][field]=name&searchCriteria[filterGroups][0][filters][0][value]=" . $q . "&searchCriteria[pageSize]=4";

            $searchCriteriaUrl = str_replace(" ", '%20', $searchCriteriaUrl);

            $ch = curl_init($searchCriteriaUrl);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
            $response = curl_exec($ch);
            $response = json_decode($response, true);
            curl_close($ch);

            $resultItems = $response['items'];
            $resultItemIds = [];
            foreach ($response['items'] as $key => $value) {
                $resultItemIds[] = $value['id'];
            }

            $product = $this->productCollFactory->create()
                ->addStoreFilter($storeId)
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', ['in' => $resultItemIds]);
            // ->addAttributeToFilter([['attribute' => 'name', 'like' => "%" . $q . "%"]])
            // ->addAttributeToFilter('status', ['eq' => 1])
            // ->setPageSize(5)
            // ->setOrder('entity_id', 'DESC');
            $orderIds = implode(",", $resultItemIds);
            $product->getSelect()->order(new \Zend_Db_Expr("FIELD(entity_id,$orderIds)"));

            if (count($product->getData()) > 0) {
                foreach ($product as $productKey => $value) {

                    $this->responseData[$this->counter]['type'] = "Product";
                    $this->responseData[$this->counter]['title'] = $value->getName();
                    $this->responseData[$this->counter]['image'] = $this->imageHelper->init($value, 'product_base_image')->getUrl();
                    $this->responseData[$this->counter]['product_url'] = $value->getProductUrl();
                    // $this->responseData[$this->counter]['price'] = $this->priceHelper->currency($value->getPrice(), true, false);
                    $this->responseData[$this->counter]['price'] = $this->priceHelper->currency($value->getFinalPrice(), true, false);
                    $this->counter++;
                }
            }
        }
        return $this->responseData;
    }

    private function getCategorySearch($responseData, $isCategorySearch, $q)
    {
        if ($isCategorySearch) {
            $category = $this->catCollFactory->create()
                ->setStore($this->storeManager->getStore())
                ->addAttributeToSelect('*')
                ->addAttributeToFilter([['attribute' => 'name', 'like' => "%" . $q . "%"]])
                ->addAttributeToFilter('is_active', ['eq' => 1]);
            if (count($category->getData()) > 0) {
                foreach ($category as $categoryKey => $value) {
                    $this->responseData[$this->counter]['type'] = "category";
                    $this->responseData[$this->counter]['title'] = $value->getName();
                    $this->responseData[$this->counter]['category_url'] = $value->getUrl();
                    $this->responseData[$this->counter]['breadcrumb'] = $this->getBreabCrumb($value->getPath())['breadcrumb'];
                    $this->responseData[$this->counter]['breadcrumb_html'] = $this->getBreabCrumb($value->getPath())['name'];
                    $this->counter++;
                }
            }
        }
        return $this->responseData;
    }

    private function getCmsSearch($responseData, $isCmsSearch, $q, $storeId)
    {
        if ($isCmsSearch) {
            $cms = $this->cmsCollFactory->create()
                ->addStoreFilter($storeId)
                ->addFieldToFilter(['title', 'content'], [['like' => "%" . $q . "%"], ['like' => "%" . $q . "%"]])
                ->addFieldToFilter('is_active', ['eq' => 1]);
            if (count($cms->getData()) > 0) {
                foreach ($cms as $cmsKey => $value) {
                    $this->responseData[$this->counter]['type'] = "cms";
                    $this->responseData[$this->counter]['title'] = $value->getTitle();
                    $this->responseData[$this->counter]['cms_url'] = $this->cmsHelper->getPageUrl($value->getId());
                    $this->counter++;
                }
            }
        }
        return $this->responseData;
    }

    private function getPopularSearch($responseData, $isPopularSearch, $q, $autocompleteData, $popSearchLimit)
    {
        if ($isPopularSearch) {
            $searchCounter = 0;
            $popularSearch = [];
            foreach ($autocompleteData as $key => $resultItem) {
                if (count($resultItem->toArray()) > 0) {
                    if ($searchCounter < $popSearchLimit) {
                        $this->responseData[$this->counter]['type'] = "popular_search";
                        $this->responseData[$this->counter]['title'] = $resultItem->getTitle();
                        $this->responseData[$this->counter]['search_url'] = $this->storeManager->getStore()->getBaseUrl() . "catalogsearch/result/?q=" . str_replace(" ", "+", $resultItem->getTitle());
                        $this->counter++;
                        $searchCounter++;
                    }
                }
            }
        }
        return $this->responseData;
    }

    private function getRecentSearch($responseData, $isRecentSearch, $q, $storeId, $recentSearchLimit)
    {
        if ($isRecentSearch) {
            $query = $this->searchQueryColl->create();
            $query->addStoreFilter($storeId);
            $query->setRecentQueryFilter()->setPageSize($recentSearchLimit);
            $query->getSelect()->where('num_results > 0 AND display_in_terms = 1');
            if (count($query->getData()) > 0) {
                $searchCounter = 0;
                $recentSearchArr = [];
                foreach ($query as $key => $resultItem) {
                    if ($searchCounter <= $recentSearchLimit) {
                        $this->responseData[$this->counter]['type'] = "recent_search";
                        $this->responseData[$this->counter]['title'] = $resultItem->getQueryText();
                        $this->responseData[$this->counter]['search_url'] = $this->storeManager->getStore()->getBaseUrl() . "catalogsearch/result/?q=" . str_replace(" ", "+", $resultItem->getQueryText());
                        $this->counter++;
                    }
                }
            }
        }
        return $this->responseData;
    }

    private function getBreabCrumb($categoryPath)
    {
        $breadcrumb = "";
        $breadcrumbArr = [];
        $breadCrumbList = [];
        foreach (explode("/", $categoryPath) as $key => $value) {
            if ($value != 1 && $value != 2) {
                $category = $this->categoryRepo->get($value);
                $breadcrumb .= $category->getName() . " > ";
                $breadCrumbList[] = $category->getName();
            }
        }
        $breadcrumbArr['breadcrumb'] = $breadCrumbList;
        $breadcrumbArr['name'] = rtrim($breadcrumb, " > ");
        return $breadcrumbArr;
    }
}
