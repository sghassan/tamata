<?php

namespace CAT\OfferPage\Model\WebApi;

use CAT\OfferPage\Api\Data\BannerDetailsInterface;

class BannerDetails extends \Magento\Framework\DataObject implements BannerDetailsInterface
{
    /**
     * {@inheritdoc}
     */
    public function getPageType()
    {
        return $this->getData(BannerDetailsInterface::PAGE_TYPE);
    }

    /**
     * {@inheritdoc}
     */
    public function setPageType($pageType)
    {
        return $this->setData(BannerDetailsInterface::PAGE_TYPE, $pageType);
    }

    /**
     * {@inheritdoc}
     */
    public function getDataId()
    {
        return $this->getData(BannerDetailsInterface::DATA_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function setDataId($dataId)
    {
        return $this->setData(BannerDetailsInterface::DATA_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getImageUrl()
    {
        return $this->getData(BannerDetailsInterface::IMAGE_URL);
    }

    /**
     * {@inheritdoc}
     */
    public function setImageUrl($imageUrl)
    {
        return $this->setData(BannerDetailsInterface::IMAGE_URL, $imageUrl);
    }
}
