<?php

namespace CAT\OfferPage\Api;

interface OfferPageInterface
{
    /**
     * @api
     * @param int $offerId
     * @return \CAT\OfferPage\Api\Data\CustomMessageInterface
     */
    public function getOfferPage($offerId);
}
