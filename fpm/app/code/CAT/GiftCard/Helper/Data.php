<?php

namespace CAT\GiftCard\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface as StoreManager;
use Magento\Framework\Message\ManagerInterface;
use Magento\CustomerBalance\Model\BalanceFactory;
use Magento\Customer\Api\CustomerRepositoryInterface as CustomerRepository;
use Magento\Framework\Exception\NoSuchEntityException;
use CAT\GiftCard\Model\ResourceModel\Coupon\CollectionFactory;
use CAT\GiftCard\Model\GiftCardRuleFactory;
use Magento\Framework\App\ResourceConnection;
use CAT\GiftCard\Model\Rule\CustomerFactory as RuleCustomerFactory;
use Magento\GiftCardAccountGraphQl\Model\Money\Formatter as MoneyFormatter;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var StoreManager
     */
    protected $_storeManager;

    /**
     * @var BalanceFactory
     */
    protected $_balanceFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @var CustomerRepository
     */
    protected $customerRepository;

    /**
     * @var CollectionFactory
     */
    protected $couponCollectionFactory;

    /**
     * @var GiftCardRuleFactory
     */
    protected $giftCardRuleFactory;

    /**
     * @var ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var RuleCustomerFactory
     */
    protected $ruleCustomerFactory;

    /**
     * @var MoneyFormatter
     */
    private $moneyFormatter;

    /**
     * Data constructor.
     * @param Context $context
     * @param StoreManager $storeManager
     * @param BalanceFactory $balanceFactory
     * @param ManagerInterface $messageManager
     * @param CustomerRepository $customerRepository
     * @param CollectionFactory $couponCollectionFactory
     * @param GiftCardRuleFactory $giftCardRuleFactory
     * @param ResourceConnection $resourceConnection
     * @param RuleCustomerFactory $ruleCustomerFactory
     */
    public function __construct(
        Context $context,
        StoreManager $storeManager,
        BalanceFactory $balanceFactory,
        ManagerInterface $messageManager,
        CustomerRepository $customerRepository,
        CollectionFactory $couponCollectionFactory,
        GiftCardRuleFactory $giftCardRuleFactory,
        ResourceConnection $resourceConnection,
        RuleCustomerFactory $ruleCustomerFactory,
        MoneyFormatter $moneyFormatter
    ) {
        parent::__construct($context);
        $this->_storeManager = $storeManager;
        $this->_balanceFactory = $balanceFactory;
        $this->messageManager = $messageManager;
        $this->customerRepository = $customerRepository;
        $this->couponCollectionFactory = $couponCollectionFactory;
        $this->giftCardRuleFactory = $giftCardRuleFactory;
        $this->resourceConnection = $resourceConnection;
        $this->ruleCustomerFactory = $ruleCustomerFactory;
        $this->moneyFormatter = $moneyFormatter;
    }

    /**
     * @param $code
     * @param $customerId
     * @return bool
     */
    public function validateGiftCard($code, $customerId) {
        if($code) {
            $collection = $this->couponCollectionFactory->create();
            $collection->addFieldToFilter('code', ['eq' => trim($code)]);
            $collection->getSelect()->joinLeft(
                ['gift_rule' => 'giftcard_rule'],
                'gift_rule.rule_id = main_table.rule_id',
                [
                    'rule_id',
                    'rule_name',
                    'description',
                    'from_date',
                    'to_date',
                    'is_active',
                    'discount_amount',
                    'gift_times_used' => 'times_used',
                    'uses_per_coupon',
                    'uses_per_customer'
                ]
            );
            $collection->getSelect()->joinLeft(['gcu' => 'giftcard_coupon_usage'], 'gcu.coupon_id = main_table.coupon_id', ['gcu_customer_id' => 'customer_id', 'gcu_times_used' => 'times_used']);
            //$collection->getSelect()->joinLeft(['gcc' => 'giftcard_customer'], 'gcc.rule_id = main_table.rule_id', ['gcc_rule_id' => 'rule_id', 'gcc_customer_id' => 'customer_id', 'gcc_times_used' => 'times_used']);
            if ($collection->getSize()) {
                $couponRules = $collection->getFirstItem();
                if (!$couponRules->getIsActive()) {
                    return false;
                }
                if (!empty($couponRules->getToDate()) && (strtotime(date('Y-m-d h:i:s')) > strtotime($couponRules->getToDate())) ) {
                    return false;
                }
                if ($this->checkIfCustomerUsedRule($couponRules->getRuleId(), $customerId)) {
                    return false;
                }
                if ($couponRules->getUsagePerCustomer() === $couponRules->getGcuTimesUsed()) {
                    return false;
                }
                if ($couponRules->getCustomerId() === $customerId) {
                    return false;
                }
                return true;
            } else {
                return false;
            }
            return false;
        }
        return false;
    }

    public function checkIfCustomerUsedRule($ruleId, $customerId) {
        $connection = $this->resourceConnection->getConnection();
        $query = $connection->select()->from('giftcard_customer')->where('rule_id=?', $ruleId)->where('customer_id=?', $customerId);
        $result = $connection->fetchAll($query);
        if (!empty($result)) {
            return true;
        }
        return false;
    }

    /**
     * @param $code
     * @param $customerId
     * @return bool
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function redeemGiftCard($code, $customerId) {
        $collection = $this->couponCollectionFactory->create();
        $collection->addFieldToFilter('code', ['eq' => $code]);
        $collection->getSelect()->joinLeft(['gift_rule' => 'giftcard_rule'], 'gift_rule.rule_id = main_table.rule_id',
            ['gift_card_rule_id' => 'rule_id', 'discount_amount']
        );

        if ($collection->getSize()) {
            $ruleData = $collection->getFirstItem();
            $currentWebsiteId = $this->_storeManager->getStore()->getWebsiteId();
            $currentWebsiteId = !empty($currentWebsiteId) ? $currentWebsiteId : 1;
            try {
                $customer = $this->customerRepository->getById($customerId);
                $balance = $this->_balanceFactory->create();
                $balance->setCustomer($customer)
                    ->setWebsiteId($currentWebsiteId)
                    ->setAmountDelta($ruleData->getDiscountAmount())
                    ->setHistoryAction(
                        \Magento\CustomerBalance\Model\Balance\History::ACTION_UPDATED
                    )
                    ->setUpdatedActionAdditionalInfo(__('Amount added for the gift card code %1', $ruleData->getCode()))
                    ->save();
                if($ruleData->getTimesUsed() >= 0) {
                    $ruleData->setTimesUsed($ruleData->getTimesUsed() + 1)->save();
                }
                $increment = true;
                $this->updateUsedCouponDetails($increment, $ruleData, $customerId);
                return true;
            } catch (NoSuchEntityException $e) {
                return false;
            }
        }
        return false;
    }

    /**
     * @param $increment
     * @param $ruleData
     * @param $customerId
     * @throws \Exception
     */
    public function updateUsedCouponDetails($increment, $ruleData, $customerId) {
        /*update the rule used time*/
        $ruleId = $ruleData->getRuleId();
        $giftCardRule = $this->giftCardRuleFactory->create()->load($ruleId);
        $giftCardRule->setTimesUsed($giftCardRule->getTimesUsed())->save();
        /*Update customer coupon used*/
        /** @var \CAT\GiftCard\Model\Rule\Customer $ruleCustomer */
        $ruleCustomer = $this->ruleCustomerFactory->create();
        $ruleCustomer->loadByCustomerRule($customerId, $ruleId);
        if ($ruleCustomer->getId()) {
            if ($increment || $ruleCustomer->getTimesUsed() > 0) {
                $ruleCustomer->setTimesUsed($ruleCustomer->getTimesUsed() + ($increment ? 1 : -1));
            }
        } elseif ($increment) {
            $ruleCustomer->setCustomerId($customerId)->setRuleId($ruleId)->setTimesUsed(1);
        }
        $ruleCustomer->save();
        //$this->customerUsage->loadByCustomerRule($customerId, $ruleData->getRuleId());
        $connection = $this->resourceConnection->getConnection();
        $connection->insert('giftcard_coupon_usage', ['coupon_id' => $ruleData->getCouponId(), 'customer_id' => $customerId, 'times_used' => '1']);
    }

    public function quickCheckCustomGiftCard($subject) {
        $code = $subject->getRequest()->getParam('giftcard_code');
        if (!empty($code)) {
            $couponCollection = $this->couponCollectionFactory->create()->addFieldToFilter('code', ['eq' => trim($code)]);
            //echo "<pre>"; print_r($couponCollection->getData()); echo "</pre>";
            //echo $couponCollection->getSelect();
            //var_dump($couponCollection->getSize());
            if ($couponCollection->getSize()) {
                $couponObj = $couponCollection->getFirstItem();
                return $couponObj;
//                echo "<pre>"; print_r($couponObj->getData()); echo "</pre>";
//                die('+++++');
            }
        }
    }

    /**
     * @param $code
     * @param $customerId
     * @param $store
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function redeemedGiftCardGetByCode($code, $customerId, $store) {
        $collection = $this->couponCollectionFactory->create();
        $collection->addFieldToFilter('code', ['eq' => trim($code)]);
        $collection->addFieldToSelect(['coupon_id', 'rule_id', 'code']);

        $collection->getSelect()->joinLeft(
            ['gift_rule' => 'giftcard_rule'],
            'gift_rule.rule_id = main_table.rule_id', [
                'rule_id', 'to_date', 'discount_amount'
            ]
        );
        $collection->getSelect()
            ->joinLeft(['gcu' => 'giftcard_coupon_usage'], 'gcu.coupon_id = main_table.coupon_id')
            ->where('gcu.customer_id=?', $customerId);
        $balance = $this->_balanceFactory->create();
        $balance->setCustomerId($customerId)->loadByCustomer();
        if ($collection->getSize()) {
            $giftCardAccount = $collection->getFirstItem();
            return [
                'code' => $giftCardAccount->getCode(),
                'balance' => $this->moneyFormatter->formatAmountAsMoney(0, $store),
                'expiration_date' => $giftCardAccount->getToDate(),
                'store_credit' => $balance->getAmount()
            ];
        }
        return [
            'code' => $code,
            'balance' => $this->moneyFormatter->formatAmountAsMoney(0, $store),
            'expiration_date' => null,
            'store_credit' => $balance->getAmount()
        ];
    }

    /**
     * @param $code
     * @param $store
     * @return array|false
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCustomGiftCardAccountDetails($code, $store) {
        $collection = $this->couponCollectionFactory->create();
        $collection->addFieldToFilter('code', ['eq' => trim($code)]);
        $collection->addFieldToSelect(['coupon_id', 'rule_id', 'code', 'times_used']);

        $collection->getSelect()->joinLeft(
            ['gift_rule' => 'giftcard_rule'],
            'gift_rule.rule_id = main_table.rule_id', [
                'rule_id', 'to_date', 'discount_amount'
            ]
        );

        if ($collection->getSize()) {
            $coupon = $collection->getFirstItem();
            if ($coupon->getTimesUsed() > 0) {
                return [
                    'code' => $coupon->getCode(),
                    'balance' => $this->moneyFormatter->formatAmountAsMoney(0, $store),
                    'expiration_date' => $coupon->getToDate(),
                    'store_credit' => null
                ];
            } else {
                return [
                    'code' => $coupon->getCode(),
                    'balance' => $this->moneyFormatter->formatAmountAsMoney($coupon->getDiscountAmount(), $store),
                    'expiration_date' => $coupon->getToDate(),
                    'store_credit' => null
                ];
            }
        }
        return false;
    }
}
