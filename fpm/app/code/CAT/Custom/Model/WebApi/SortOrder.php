<?php

namespace CAT\Custom\Model\WebApi;

use Magento\Catalog\Api\ProductRepositoryInterfaceFactory;

class SortOrder implements \CAT\Custom\Api\SortOrderInterface
{
    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    protected $_request;

    /**
     * @var \Magento\Framework\App\ResourceConnection
     */
    protected $resourceConnection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ProductRepositoryInterfaceFactory
     */
    protected $_productRepositoryFactory;

    /**
     * @var \Magento\Framework\DB\Adapter\AdapterInterface
     */
    protected $connection;

    /**
     * @param \Magento\Framework\Webapi\Rest\Request $request
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param ProductRepositoryInterfaceFactory $productRepositoryFactory
     */
    public function __construct(
        \Magento\Framework\Webapi\Rest\Request $request,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        ProductRepositoryInterfaceFactory $productRepositoryFactory
    ) {
        $this->_request = $request;
        $this->resourceConnection = $resourceConnection;
        $this->_storeManager = $storeManager;
        $this->_productRepositoryFactory = $productRepositoryFactory;
        $this->connection = $this->resourceConnection->getConnection();
    }

    /**
     * @inheritDoc
     */
    public function getOrders()
    {
        $sku = $this->_request->getParam('sku');
        $page = !empty($this->_request->getParam('page_size')) ? $this->_request->getParam('page_size') : null;
        $rowCount = !empty($this->_request->getParam('current_page')) ? $this->_request->getParam('current_page') : 0;
        $orderBy = !empty($this->_request->getParam('order_by')) ? $this->_request->getParam('order_by') : null;
        $dir = !empty($this->_request->getParam('dir')) ? ' '.$this->_request->getParam('dir') : null;
        //
        try {
            if (array_key_exists('sku', $this->_request->getparams()) && !empty($sku)) {
                $selectQuery = $this->connection->select()
                    ->from('sales_order_item', ['order_id'])
                    ->where('sku=?', $sku)
                    ->order('created_at desc')
                    ->where('parent_item_id IS NULL');
                $result = $this->connection->fetchCol($selectQuery);
                $totalCount = count($result);
                if (!empty($result)) {
                    $where = 'entity_id IN ('.implode(',', $result).')';
                    $orderSelect = $this->connection->select()
                        ->from(['so' => 'sales_order'], ['so.entity_id', 'so.increment_id', 'so.total_item_count', 'so.created_at', 'so.sorting_history', 'so.customer_email'])
                        ->where($where)
                        ->limit($page, $page*$rowCount);
                    $orderSelect->order('created_at desc');
                    $orderResult = $this->connection->fetchAll($orderSelect);

                    $_order = [];
                    foreach ($orderResult as $order) {
                        /** Get Sub Order Data */
                        $_order[$order['entity_id']] = $this->getSubOrderData($order);
                    }
                }
            } elseif(array_key_exists('vendor_id', $this->_request->getparams())) {
                $vendorId = $this->_request->getparam('vendor_id');
                $select = $this->connection->select()->from(['mvo' => 'md_vendor_order'], 'mvo.order_id');
                //$select->joinLeft(['mvwd' => 'md_vendor_website_data'], 'mvo.vendor_id = mvwd.vendor_id', '');
                $select->group('mvo.order_id');
                $select->where('mvo.vendor_id=?',$vendorId);
                if (array_key_exists('item_status', $this->_request->getparams())) {
                    $select->where('mvo.status=?',$this->_request->getparam('item_status'));
                } else {
                    //Adding a status array if item_status value is empty
                    $itemStatus = ["pending", "confirmed", "processing"];
                    $where = "mvo.status IN ('".implode("','", $itemStatus)."')";
                    $select->where($where);
                }

                $result = $this->connection->fetchCol($select);
                $totalCount = count($result);
                if (!empty($result)) {
                    $where = 'entity_id IN ('.implode(',', $result).')';
                    $orderSelect = $this->connection->select();
                    $orderSelect->from(['so' => 'sales_order'], ['so.entity_id', 'so.increment_id', 'so.total_item_count', 'so.created_at', 'so.sorting_history', 'so.customer_email']);
                    if (!empty($orderBy) && $orderBy === 'sku') {
                        $orderSelect->joinLeft(['soi' => 'sales_order_item'], 'soi.order_id = so.entity_id', ['']);
                        $orderSelect->group('so.entity_id');
                        $orderSelect->order($orderBy . $dir);
                    } else {
                        $orderSelect->order('created_at desc');
                    }
                    $orderSelect->where($where);
                    $orderSelect->limit($page, $page*$rowCount);
                    $orderResult = $this->connection->fetchAll($orderSelect);

                    $_order = [];
                    foreach ($orderResult as $order) {
                        /** Get Sub Order Data */
                        $_order[$order['entity_id']] = $this->getSubOrderData($order);
                    }
                }
            } elseif(array_key_exists('item_status', $this->_request->getParams())) {
                $itemStatus = $this->_request->getParam('item_status');
                $select = $this->connection->select()->from(['mvo' => 'md_vendor_order'], 'mvo.order_id');
                $select->group('mvo.order_id');
                $select->where('mvo.status=?',$itemStatus);
                $result = $this->connection->fetchCol($select);
                $totalCount = count($result);
                if (!empty($result)) {
                    $where = 'entity_id IN ('.implode(',', $result).')';
                    $orderSelect = $this->connection->select()
                        ->from(['so' => 'sales_order'], ['so.entity_id', 'so.increment_id', 'so.total_item_count', 'so.created_at', 'so.sorting_history', 'so.customer_email'])
                        ->order('created_at desc')
                        ->where($where)
                        ->limit($page, $page*$rowCount);
                    $orderResult = $this->connection->fetchAll($orderSelect);
                    $_order = [];
                    foreach ($orderResult as $order) {
                        /** Get Sub Order Data */
                        $_order[$order['entity_id']] = $this->getSubOrderData($order);
                    }
                }
            } elseif (array_key_exists('orderId', $this->_request->getParams()) && !empty($this->_request->getParam('orderId'))) {
                $totalCount = 1;
                $orderId = $this->_request->getParam('orderId');
                $orderSelect = $this->connection->select()
                    ->from(['so' => 'sales_order'], ['so.entity_id', 'so.increment_id', 'so.total_item_count', 'so.created_at', 'so.sorting_history', 'so.customer_email'])
                    ->order('created_at desc')
                    ->where('increment_id=?', $orderId)
                    ->limit($page, $page*$rowCount);
                $orderResult = $this->connection->fetchAll($orderSelect);
                $totalCount = count($orderResult);
                $_order = [];
                foreach ($orderResult as $order) {
                    /** Get Sub Order Data */
                    $_order[$order['entity_id']] = $this->getSubOrderData($order);
                }
            }
            $_order['search']['total'] = $totalCount;
            $_order['search']['page_size'] = $page;
            $_order['search']['current_page'] = $rowCount;
            $_order['search']['order_by'] = $orderBy;
            $_order['search']['dir'] = $dir;
            return $_order;
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * @param $order
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSubOrderData($order) {
        $mediaUrl = $this->_storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA).'catalog/product';
        $subOrderQuery = $this->connection->select()
            ->from(['mvo' => 'md_vendor_order'], ['increment_id', 'status', 'vendor_order_with_classification', 'is_sorted', 'is_sorted_timestamp', 'is_picked_up_timestamp'])
            ->joinLeft(['so' => 'sales_order_item'], 'so.vendor_order_id = mvo.vendor_order_id', ['product_id', 'name', 'sku', 'qty_ordered', 'product_options', 'product_type', 'price', 'original_price'])
            ->joinLeft(['mvwd' => 'md_vendor_website_data'], 'mvwd.vendor_id = mvo.vendor_id', ['vendor_name' => 'mvwd.name'])
            ->joinLeft(['cpe' => 'catalog_product_entity'], 'cpe.entity_id = so.product_id', '')
            ->joinLeft(['cpev' => 'catalog_product_entity_varchar'], 'cpe.row_id = cpev.row_id AND cpev.attribute_id = 87', '')
            ->joinLeft(['in_warehouse' => 'catalog_product_entity_varchar'], 'cpe.row_id = in_warehouse.row_id AND in_warehouse.attribute_id = 432', ['in_warehouse' => 'in_warehouse.value'])
            ->columns("CONCAT('".$mediaUrl."', cpev.value) as image_url")
            ->columns(['attr_json' => new \Zend_Db_Expr('JSON_EXTRACT(so.product_options, "$.attributes_info")')])
            ->joinLeft(['cpe1' => 'catalog_product_entity'], 'cpe1.sku = so.sku', '')
            ->joinLeft(['cpev1' => 'catalog_product_entity_varchar'], 'cpe1.row_id = cpev1.row_id AND cpev1.attribute_id = 87', '')
            ->columns("CONCAT('".$mediaUrl."', cpev1.value) as product_image")
            ->where('mvo.order_id=?', $order['entity_id'])
            ->where('so.parent_item_id IS NULL');
        if (array_key_exists('sku', $this->_request->getparams())) {
            $itemStatus = ["pending", "confirmed", "processing"];
            $where = "mvo.status IN ('".implode("','", $itemStatus)."')";
            $subOrderQuery->where($where);
        }
        //echo $subOrderQuery; die();
        $subOrderResult =  $this->connection->fetchAll($subOrderQuery);

        $_orders['entity_id'] = $order['entity_id'];
        $_orders['increment_id'] = $order['increment_id'];
        $_orders['total_item_count'] = $order['total_item_count'];
        $_orders['created_at'] = $order['created_at'];
        $_orders['sorting_history'] = $order['sorting_history'];
        $_orders['customer_email'] = $order['customer_email'];
        $_orders['items'] = $subOrderResult;
        return $_orders;
    }
}
