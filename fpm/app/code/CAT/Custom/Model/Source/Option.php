<?php
namespace CAT\Custom\Model\Source;

use Magento\Framework\DataObject;
use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Option
 * @package CAT\Custom\Model\Source
 */
class Option extends DataObject implements OptionSourceInterface
{
    const STORE_CREDIT_KEYWORD = 'store_credit';

    /*const INVOICE_SHIPMENT_KEYWORD = 'invoice_shipment';*/

    /*const VENDOR_PAYMENT_STATUS = 'vendor_payment_status';*/

    const ATTRIBUTE_LIST = [
        self::STORE_CREDIT_KEYWORD,
        /*self::INVOICE_SHIPMENT_KEYWORD,*/
        /*self::VENDOR_PAYMENT_STATUS*/
    ];

    /**
     * Option constructor.
     * @param array $data
     */
    public function __construct(
        array $data = []
    ) {
        parent::__construct($data);
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $optionArray = [
            '' => __('Please Select'),
            self::STORE_CREDIT_KEYWORD =>  __('Store Credit'),
            /*self::INVOICE_SHIPMENT_KEYWORD => __('Invoice/Shipment'),
            self::VENDOR_PAYMENT_STATUS => __('Vendor Payment')*/
        ];
        return $optionArray;
    }
}
