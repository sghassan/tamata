<?php

declare (strict_types = 1);

namespace Ktpl\Pushnotification\Model;

use Ktpl\Pushnotification\Api\KtplPushnotificationListInterface;
use Magento\Framework\Exception\LocalizedException;

class KtplPushnotificationList implements KtplPushnotificationListInterface
{
    /**
     * @var \Ktpl\Pushnotification\Model\KtplPushnotificationsFactory
     */
    protected $pushNotifyFactory;

    /**
     * @var \Ktpl\ExtendedPushNotification\Model\KtplPushNotificationTransactionalFactory
     */
    protected $ktplPushNTFactory;

    /**
     * @var \Ktpl\Pushnotification\Model\Data\KtplPushnotificationData
     */
    protected $pushNotifyData;

    /**
     * @var \Ktpl\Pushnotification\Model\Data\KtplPushnotificationObjFactory
     */
    protected $pushNotifyObjData;

    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $dataObjectFactory;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $faviconIcon;

    /**
     * @var \Magento\Framework\DataObjectFactory
     */
    protected $assetRepo;

    /**
     * @param \Ktpl\Pushnotification\Model\KtplPushnotificationsFactory                     $pushNotifyFactory
     * @param \Ktpl\ExtendedPushNotification\Model\KtplPushNotificationTransactionalFactory $ktplPushNTFactory
     * @param \Ktpl\Pushnotification\Model\Data\KtplPushnotificationDataFactory             $pushNotifyData
     * @param \Ktpl\Pushnotification\Model\Data\KtplPushnotificationObjFactory              $pushNotifyObjData
     * @param \Magento\Customer\Model\CustomerFactory                                       $customerFactory
     * @param \Magento\Framework\DataObjectFactory                                          $dataObjectFactory
     */
    public function __construct(
        \Ktpl\Pushnotification\Model\KtplPushnotificationsFactory $pushNotifyFactory,
        \Ktpl\ExtendedPushNotification\Model\KtplPushNotificationTransactionalFactory $ktplPushNTFactory,
        \Ktpl\Pushnotification\Model\Data\KtplPushnotificationDataFactory $pushNotifyData,
        \Ktpl\Pushnotification\Model\Data\KtplPushnotificationObjFactory $pushNotifyObjData,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Theme\Model\Favicon\Favicon $faviconIcon,
        \Magento\Framework\View\Asset\Repository $assetRepo
    ) {
        $this->pushNotifyFactory = $pushNotifyFactory;
        $this->ktplPushNTFactory = $ktplPushNTFactory;
        $this->pushNotifyData = $pushNotifyData;
        $this->pushNotifyObjData = $pushNotifyObjData;
        $this->customerFactory = $customerFactory;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->faviconIcon = $faviconIcon;
        $this->assetRepo = $assetRepo;
    }

    /**
     * Get All Push Notification List By Customer Id
     * @param int $customerId The Customer ID.
     * @return \Ktpl\Pushnotification\Api\Data\KtplPushnotificationDataInterface
     * @throws \Magento\Framework\Exception\LocalizedException.
     */
    public function getNotificationList($customerId)
    {
        $customer = $this->customerFactory->create()->load($customerId);
        $items = [];
        $counter = 0;
        $pushNotifyDataObj = $this->pushNotifyData->create();
        /*Stop using the API because of load...*/
        $pushNotifyDataObj = $this->pushNotifyData->create();
        $pushNotifyDataObj->setNotifications([]);
        return $pushNotifyDataObj;
        /*Stop using the API because of load...*/
        try {
            if (!$customer) {
                $invalid = [
                    [
                        "message" => __('Customer not exist'),
                    ],
                ];
                return $invalid;
            }

            $to = date("Y-m-d h:i:s"); // current date
            $from = strtotime('-15 day', strtotime($to));
            $from = date('Y-m-d h:i:s', $from); // 15 days before
            $pushFactory = $this->pushNotifyFactory->create()->getCollection();
            // $pushFactory->addFieldToFilter('send_to_customer', ['finset' => $customer->getEmail()])
            $pushFactory->addFieldToFilter(['send_to_customer','send_to_customer_group'],
                    [
                        ['finset' => $customer->getEmail()],
                        ['finset' => $customer->getGroupId()],
                    ]
                )
                ->addFieldToFilter('created_at', array('from'=>$from))
                ->addFieldToFilter('is_sent', ['in' => array(0,3)]);
            $ktplPushNTFObj = $this->ktplPushNTFactory->create()
                ->getCollection()
                ->addFieldToFilter('customer_email', ['eq' => $customer->getEmail()])
                // ->addFieldToFilter('created_at', ['lteq' => $to])
                // ->addFieldToFilter('created_at', ['gteq' => $from]);
                // ->addFieldToFilter('created_at', array('from'=>$from, 'to'=>$to));
                ->addFieldToFilter('created_at', array('from'=>$from));
            foreach ($pushFactory as $model) {
                $pushNotifyData = $this->pushNotifyObjData->create();
                $pushNotifyData->setNotificationId($model->getId());

                // $pushNotifyData->setNotificationTitle($model->getTitle());
                // $pushNotifyData->setNotificationDescription($model->getDescription());
                $pushNotifyData->setNotificationTitle($this->parseLocalNotificationVariables($customerId,$model->getTitle()) );
                $pushNotifyData->setNotificationDescription($this->parseLocalNotificationVariables($customerId,$model->getDescription()) );

                $pushNotifyData->setRedirectionTypeid($model->getPromotionId());
                $nType = "";
                if ($model->getTypePromotion() == "category") {
                    $nType = "category_promotions";
                }if ($model->getTypePromotion() == "microsite") {
                    $nType = "microsite_promotions";
                }if ($model->getTypePromotion() == "product") {
                    $nType = "product_promotions";
                }
                $pushNotifyData->setNotificationType($nType);
                $pushNotifyData->setNotificationImgUrl($model->getImageUrl());
                $pushNotifyData->setNotificationCreatedAt($model->getCreatedAt());
                $items[] = $pushNotifyData;
            }

            foreach ($ktplPushNTFObj as $key => $value) {
                $pushNotifyData = $this->pushNotifyObjData->create();
                $pushNotifyData->setNotificationId($value->getId());
                $pushNotifyData->setNotificationTitle($value->getTitle());
                $pushNotifyData->setNotificationDescription($value->getDescription());
                $pushNotifyData->setRedirectionTypeid($value->getPromotionId());
                $pushNotifyData->setNotificationType($value->getTypePromotion());
                // $pushNotifyData->setNotificationImgUrl($value->getImageUrl());
                $pushNotifyData->setNotificationCreatedAt($value->getCreatedAt());
                $items[] = $pushNotifyData;
            }

            /* get pushnotification which is sends by selecting to All */
            $pushFactoryofAll = $this->pushNotifyFactory->create()->getCollection();
            $pushFactoryofAll->addFieldToFilter('send_to_customer_group', ['null' => true])
                ->addFieldToFilter('send_to_customer', ['null' => true])
                ->addFieldToFilter('created_at', array('from'=>$from))
                ->addFieldToFilter('is_sent', ['in' => array(0,3)]);

            foreach ($pushFactoryofAll as $model) {
                $pushNotifyData = $this->pushNotifyObjData->create();
                $pushNotifyData->setNotificationId($model->getId());

                // $pushNotifyData->setNotificationTitle($model->getTitle());
                // $pushNotifyData->setNotificationDescription($model->getDescription());
                $pushNotifyData->setNotificationTitle($this->parseLocalNotificationVariables($customerId,$model->getTitle()) );
                $pushNotifyData->setNotificationDescription($this->parseLocalNotificationVariables($customerId,$model->getDescription()) );

                $pushNotifyData->setRedirectionTypeid($model->getPromotionId());
                $nType = "";
                if ($model->getTypePromotion() == "category") {
                    $nType = "category_promotions";
                }if ($model->getTypePromotion() == "microsite") {
                    $nType = "microsite_promotions";
                }if ($model->getTypePromotion() == "product") {
                    $nType = "product_promotions";
                }
                $pushNotifyData->setNotificationType($nType);
                $pushNotifyData->setNotificationImgUrl($model->getImageUrl());
                $pushNotifyData->setNotificationCreatedAt($model->getCreatedAt());
                $items[] = $pushNotifyData;
            }

            /* get pushnotification which is sends by selecting to All */

            $sortByDateDesc = array();

            foreach ($items as $key => $value){
                $sortByDateDesc[] = strtotime($value->getNotificationCreatedAt());
            }
            array_multisort($sortByDateDesc, SORT_DESC, $items);

        } catch (\Exception $exception) {
            throw new LocalizedException(__(
                'Could not save the Pushnotifications: %1',
                $exception->getMessage()
            ));
        }
        $pushNotifyDataObj->setNotifications($items);
        return $pushNotifyDataObj;
    }

    public function parseLocalNotificationVariables($customerId,$message)
    {
        $customerObject = $this->customerFactory->create()->load($customerId);

        foreach ($this->getReplaceableVariables() as $value) {
            if (strpos($message, $value) !== false) {
                switch ($value) {
                    case '{{first_name}}':
                        $message = str_replace($value, $customerObject->getFirstname(), $message);
                        break;
                    case '{{last_name}}':
                        $message = str_replace($value, $customerObject->getLastname(), $message);
                        break;
                    case '{{full_name}}':
                        $message = str_replace($value, $customerObject->getFullname(), $message);
                        break;
                    // case '{{order_id}}':
                    //     $message = str_replace($value, $this->orderId, $message);
                    //     break;
                    // case '{{invoice_id}}':
                    //     $message = str_replace($value, $this->observer->getIncrementId(), $message);
                    //     break;
                    // case '{{shipping_id}}':
                    //     $message = str_replace($value, $this->observer->getIncrementId(), $message);
                    //     break;
                    // case '{{product_name}}':
                    //     $message = str_replace($value, $this->recentProductName, $message);
                    //     break;
                    // case '{{cart_id}}':
                    //     //$message = str_replace($value, $this->cartId(), $message);
                    //     $message = str_replace($value, $this->typeId, $message);
                    //     break;
                }
            }
        }

         return $message;
    }

    public function getReplaceableVariables()
    {
        return [
            '{{first_name}}',
            '{{last_name}}',
            '{{full_name}}',
            '{{order_id}}',
            '{{invoice_id}}',
            '{{shipping_id}}',
            '{{product_name}}',
            '{{cart_id}}'
        ];
    }


}
