#!/bin/bash

export webTAG="web-${DRONE_COMMIT_SHA:0:7}"
export fpmTAG="fpm-${DRONE_COMMIT_SHA:0:7}"

mkdir -p $HOME/.ssh
echo "$SSH_KEY" > $HOME/.ssh/ed25519
chmod 600 $HOME/.ssh/ed25519
ssh-keyscan bitbucket.org > /$HOME/.ssh/known_hosts
cat <<EOT >> $HOME/.ssh/config
    Host bitbucket.org
    HostName bitbucket.org
    User git
    IdentityFile ~/.ssh/ed25519
EOT

echo "${PROJECT:0:3}-helm-charts repository cloning"

if git clone git@bitbucket.org:creativeadvtech/${PROJECT:0:3}-helm-charts.git helm-charts; then
    cd helm-charts
else
    exit 1
fi

git checkout master && git branch --set-upstream-to=origin master

yq e -i '(.magentoWeb.image.tag) = strenv(webTAG)' magento/values-${ENV}.yaml
yq e -i '(.magentoFpm.image.tag) = strenv(fpmTAG)' magento/values-${ENV}.yaml
helm secrets lint -f magento/values-${ENV}.yaml -f secrets://magento/secrets-${ENV}.enc.yaml magento/

if git add --all && git commit -m "magento image tags updated to ${webTAG}" && git push -u origin master; then
    echo "**SUCCESS** Magento app will be deployed soon"
    argocd login --username $ARGOCD_USERNAME --password $ARGOCD_PASSWORD argocd.creativeadvtech.ml
    argocd app get --grpc-web ${PROJECT}-${ENV}-magento --hard-refresh
    # argocd app wait --grpc-web ${PROJECT}-${ENV}-magento
else
    echo "**WARNING** nothing has changed, exit"
fi
